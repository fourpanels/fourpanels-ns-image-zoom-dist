import { Property, View } from '@nativescript/core';
export class ImageZoomBase extends View {
}
export const resizeProperty = new Property({
    name: 'resize'
});
export const stretchProperty = new Property({
    name: 'stretch'
});
export const zoomScaleProperty = new Property({
    name: 'zoomScale',
    defaultValue: 1
});
export const minZoomScaleProperty = new Property({
    name: 'minZoom',
    defaultValue: 1
});
export const maxZoomScaleProperty = new Property({
    name: 'maxZoom',
    defaultValue: 4
});
export const srcProperty = new Property({
    name: 'src'
});
srcProperty.register(ImageZoomBase);
stretchProperty.register(ImageZoomBase);
zoomScaleProperty.register(ImageZoomBase);
minZoomScaleProperty.register(ImageZoomBase);
maxZoomScaleProperty.register(ImageZoomBase);
resizeProperty.register(ImageZoomBase);
//# sourceMappingURL=image-zoom.common.js.map