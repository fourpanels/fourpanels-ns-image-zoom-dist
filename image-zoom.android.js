import { Utils, knownFolders, path } from '@nativescript/core';
import { ImageZoomBase, maxZoomScaleProperty, minZoomScaleProperty, resizeProperty, srcProperty, stretchProperty } from './image-zoom.common';
const PicassoProvider = com.squareup.picasso.provider.PicassoProvider;
const PhotoView = com.github.chrisbanes.photoview.PhotoView;
const OnViewTapListener = com.github.chrisbanes.photoview.OnViewTapListener;
export class ImageZoom extends ImageZoomBase {
    constructor() {
        super();
    }
    setScale(scale, animate = true) {
        const boundedScale = Math.max(this.minZoom, Math.min(scale, this.maxZoom));
        return this.nativeView.setScale(boundedScale, animate);
    }
    getScale() {
        return this.nativeView.getScale();
    }
    createNativeView() {
        this.picasso = PicassoProvider.get();
        const nativeView = new PhotoView(this._context);
        this.intiTapEvent(nativeView);
        return nativeView;
    }
    intiTapEvent(nativeView) {
        const viewTapListener = {
            onViewTap: (view, x, y) => {
                const eventData = {
                    eventName: 'androidViewTap',
                    object: this
                };
                this.notify(eventData);
            }
        };
        nativeView.setOnViewTapListener(new OnViewTapListener(viewTapListener));
    }
    [minZoomScaleProperty.setNative](scale) {
        if (this.nativeView && Utils.isNumber(scale)) {
            this.nativeView.setMinimumScale(scale);
            this.nativeView.setScaleLevels(Number(scale), Number(0.5833333333333334 * this.maxZoom), Number(this.maxZoom));
        }
    }
    [maxZoomScaleProperty.setNative](scale) {
        if (this.nativeView && Utils.isNumber(scale)) {
            this.nativeView.setScaleLevels(Number(this.minZoom), Number(0.5833333333333334 * scale), Number(scale));
        }
    }
    initNativeView() {
        this.nativeView.setScaleLevels(Number(this.minZoom), Number(0.5833333333333334 * this.maxZoom), Number(this.maxZoom));
        if (this.src) {
            const image = this.getImage(this.src);
            this.builder = this.picasso.load(image);
        }
        if (this.stretch) {
            this.resetImage();
        }
        if (this.builder) {
            if (this.resize &&
                this.resize !== undefined &&
                this.resize.split(',').length > 1 &&
                this.stretch !== 'fill') {
                this.builder.resize(parseInt(this.resize.split(',')[0], 10), parseInt(this.resize.split(',')[1], 10));
            }
            this.builder.into(this.nativeView);
        }
    }
    getResourceId(res = '') {
        if (res.startsWith('res://')) {
            return Utils.ad.resources.getDrawableId(res.replace('res://', ''));
        }
        return 0;
    }
    set borderRadius(value) {
        this.style.borderRadius = value;
        this.setBorderAndRadius();
    }
    set borderWidth(value) {
        this.style.borderWidth = value;
        this.setBorderAndRadius();
    }
    set borderLeftWidth(value) {
        this.style.borderLeftWidth = value;
        this.setBorderAndRadius();
    }
    set borderRightWidth(value) {
        this.style.borderRightWidth = value;
        this.setBorderAndRadius();
    }
    set borderBottomWidth(value) {
        this.style.borderBottomWidth = value;
        this.setBorderAndRadius();
    }
    set borderTopWidth(value) {
        this.style.borderTopWidth = value;
        this.setBorderAndRadius();
    }
    set borderBottomLeftRadius(value) {
        this.style.borderBottomLeftRadius = value;
        this.setBorderAndRadius();
    }
    set borderBottomRightRadius(value) {
        this.style.borderBottomRightRadius = value;
        this.setBorderAndRadius();
    }
    set borderTopLeftRadius(value) {
        this.style.borderTopLeftRadius = value;
        this.setBorderAndRadius();
    }
    set borderTopRightRadius(value) {
        this.style.borderTopRightRadius = value;
        this.setBorderAndRadius();
    }
    [srcProperty.getDefault]() {
        return undefined;
    }
    [srcProperty.setNative](src) {
        if (!this.builder) {
            const image = this.getImage(src);
            this.builder = this.picasso.load(image);
        }
        if (this.stretch) {
            this.resetImage();
        }
        this.setBorderAndRadius();
        this.builder.into(this.nativeView);
        return src;
    }
    [resizeProperty.setNative](resize) {
        if (!this.builder) {
            return resize;
        }
        if (resize &&
            resize !== undefined &&
            resize.split(',').length > 1 &&
            this.stretch !== 'fill') {
            this.builder.resize(parseInt(resize.split(',')[0], 10), parseInt(resize.split(',')[1], 10));
        }
        return resize;
    }
    getImage(src) {
        let nativeImage;
        if (Utils.isNullOrUndefined(src)) {
            return src;
        }
        if (typeof src === 'string' && src.substr(0, 1) === '/') {
            nativeImage = new java.io.File(src);
        }
        else if (typeof src === 'string' && src.startsWith('~/')) {
            nativeImage = new java.io.File(path.join(knownFolders.currentApp().path, src.replace('~/', '')));
        }
        else if (typeof src === 'string' && src.startsWith('http')) {
            nativeImage = src;
        }
        else if (typeof src === 'string' && src.startsWith('res://')) {
            nativeImage = Utils.ad.resources.getDrawableId(src.replace('res://', ''));
        }
        else if (typeof src === 'object') {
            const tempFile = path.join(knownFolders.currentApp().path, `${Date.now()} + .png`);
            const saved = src.saveToFile(tempFile, 'png');
            if (saved) {
                nativeImage = new java.io.File(tempFile);
            }
        }
        return nativeImage;
    }
    [stretchProperty.getDefault]() {
        return 'aspectFit';
    }
    [stretchProperty.setNative](value) {
        if (!this.builder)
            return value;
        this.resetImage(true);
        return value;
    }
    clearItem() {
    }
    setBorderAndRadius() {
        if (!this.builder)
            return;
        const RoundedCornersTransformation = jp.wasabeef.picasso.transformations.RoundedCornersTransformation;
        this.builder = this.builder
            .transform(new RoundedCornersTransformation(Utils.layout.toDevicePixels(this.style.borderTopLeftRadius), Utils.layout.toDevicePixels(this.style.borderTopWidth), RoundedCornersTransformation.CornerType.TOP_LEFT))
            .transform(new RoundedCornersTransformation(Utils.layout.toDevicePixels(this.style.borderTopRightRadius), Utils.layout.toDevicePixels(this.style.borderTopWidth), RoundedCornersTransformation.CornerType.TOP_RIGHT))
            .transform(new RoundedCornersTransformation(Utils.layout.toDevicePixels(this.style.borderBottomLeftRadius), Utils.layout.toDevicePixels(this.style.borderBottomWidth), RoundedCornersTransformation.CornerType.BOTTOM_LEFT))
            .transform(new RoundedCornersTransformation(Utils.layout.toDevicePixels(this.style.borderBottomRightRadius), Utils.layout.toDevicePixels(this.style.borderBottomWidth), RoundedCornersTransformation.CornerType.BOTTOM_RIGHT));
    }
    setAspectResize() {
        let newSize;
        if (this.resize &&
            this.resize !== undefined &&
            this.resize.split(',').length > 1) {
            newSize = {
                width: parseInt(this.resize.split(',')[0], 10),
                height: parseInt(this.resize.split(',')[1], 10)
            };
        }
        else if (this.width || this.height) {
            newSize = {
                width: parseInt(this.width.toString(), 10),
                height: parseInt(this.height.toString(), 10)
            };
        }
        else {
            newSize = {
                width: this.parent.effectiveWidth,
                height: this.parent.effectiveHeight
            };
        }
        this.builder.resize(newSize.width, newSize.height);
    }
    resetImage(reload = false) {
        if (!this.builder)
            return;
        switch (this.stretch) {
            case 'aspectFit':
                this.builder = this.picasso.load(this.getImage(this.src));
                this.setBorderAndRadius();
                this.setAspectResize();
                this.builder.centerInside();
                if (reload) {
                    this.builder.into(this.nativeView);
                }
                break;
            case 'aspectFill':
                this.builder = this.picasso.load(this.getImage(this.src));
                this.setBorderAndRadius();
                this.setAspectResize();
                this.builder.centerCrop();
                if (reload) {
                    this.builder.into(this.nativeView);
                }
                break;
            case 'fill':
                this.builder = this.picasso.load(this.getImage(this.src));
                this.setBorderAndRadius();
                this.builder.fit();
                if (reload) {
                    this.builder.into(this.nativeView);
                }
                break;
            case 'none':
            default:
                this.builder = this.picasso.load(this.getImage(this.src));
                this.setBorderAndRadius();
                if (reload) {
                    this.builder.into(this.nativeView);
                }
                break;
        }
    }
}
//# sourceMappingURL=image-zoom.android.js.map