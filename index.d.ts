import { ImageZoomBase } from './image-zoom.common';
export declare class ImageZoom extends ImageZoomBase {

    public getScale(): number;
    public setScale(scale: number, animate?: boolean): void;
}
