import { ImageSource, knownFolders, path, Utils } from '@nativescript/core';
import { ImageZoomBase, maxZoomScaleProperty, minZoomScaleProperty, srcProperty, stretchProperty, zoomScaleProperty } from './image-zoom.common';
export class ImageZoom extends ImageZoomBase {
    constructor() {
        super();
    }
    getScale() {
        return this.nativeView.zoomScale;
    }
    setScale(scale, animate = true) {
        const boundedScale = Math.max(this.minZoom, Math.min(scale, this.maxZoom));
        this.ios.setZoomScaleAnimated(boundedScale, animate);
    }
    createNativeView() {
        this._image = UIImageView.new();
        this._image.clipsToBounds = true;
        this._image.contentMode = 1;
        const nativeView = UIScrollView.new();
        nativeView.addSubview(this._image);
        nativeView.zoomScale = this.zoomScale;
        nativeView.minimumZoomScale = this.minZoom;
        nativeView.maximumZoomScale = this.maxZoom;
        return nativeView;
    }
    disposeNativeView() {
        this.delegate = null;
    }
    onLayout(left, top, right, bottom) {
        super.onLayout(left, top, right, bottom);
        this._image.frame = this.nativeView.bounds;
    }
    onMeasure(widthMeasureSpec, heightMeasureSpec) {
        const nativeView = this.nativeView;
        if (nativeView) {
            const width = Utils.layout.getMeasureSpecSize(widthMeasureSpec);
            const height = Utils.layout.getMeasureSpecSize(heightMeasureSpec);
            this.setMeasuredDimension(width, height);
        }
    }
    initNativeView() {
        this.delegate = UIScrollViewDelegateImpl.initWithOwner(new WeakRef(this));
        this.nativeView.delegate = this.delegate;
    }
    [stretchProperty.setNative](value) {
        switch (value) {
            case 'aspectFit':
                this.nativeViewProtected.contentMode = 1;
                break;
            case 'aspectFill':
                this.nativeViewProtected.contentMode = 2;
                break;
            case 'fill':
                this.nativeViewProtected.contentMode = 0;
                break;
            case 'none':
            default:
                this.nativeViewProtected.contentMode = 9;
                break;
        }
    }
    [srcProperty.setNative](src) {
        if (typeof src === 'string' && src.startsWith('res://')) {
            this._image.image = UIImage.imageNamed(src.replace('res://', ''));
        }
        else if (typeof src === 'object') {
            this._image.image = src.ios;
        }
        else if (typeof src === 'string' && src.startsWith('http')) {
            ImageSource.fromUrl(src).then(source => {
                this._image.image = source.ios;
            });
        }
        else if (typeof src === 'string' && src.startsWith('~')) {
            this._image.image = UIImage.imageWithContentsOfFile(path.join(knownFolders.currentApp().path, src.replace('~', '')));
        }
        else {
            this._image.image = UIImage.imageWithContentsOfFile(src);
        }
    }
    [stretchProperty.setNative](stretch) {
        this._image.stretch = stretch;
    }
    [zoomScaleProperty.setNative](scale) {
        if (this.nativeView) {
            this.nativeView.zoomScale = scale;
        }
    }
    [minZoomScaleProperty.setNative](scale) {
        if (this.nativeView) {
            this.nativeView.minimumZoomScale = scale;
        }
    }
    [maxZoomScaleProperty.setNative](scale) {
        if (this.nativeView) {
            this.nativeView.maximumZoomScale = scale;
        }
    }
}
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UIScrollViewDelegateImpl = void 0;
var UIScrollViewDelegateImpl = /** @class */ (function (_super) {
    __extends(UIScrollViewDelegateImpl, _super);
    function UIScrollViewDelegateImpl() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    UIScrollViewDelegateImpl.initWithOwner = function (owner) {
        var delegate = new UIScrollViewDelegateImpl();
        delegate.owner = owner;
        return delegate;
    };
    UIScrollViewDelegateImpl.prototype.viewForZoomingInScrollView = function (scrollView) {
        var owner = this.owner.get();
        return owner._image;
    };
    UIScrollViewDelegateImpl.ObjCProtocols = [UIScrollViewDelegate];
    return UIScrollViewDelegateImpl;
}(NSObject));
exports.UIScrollViewDelegateImpl = UIScrollViewDelegateImpl;
//# sourceMappingURL=image-zoom.ios.js.map